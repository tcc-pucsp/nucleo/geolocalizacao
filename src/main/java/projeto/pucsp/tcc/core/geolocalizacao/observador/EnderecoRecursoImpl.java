package projeto.pucsp.tcc.core.geolocalizacao.observador;

import lombok.extern.slf4j.Slf4j;
import org.springframework.amqp.rabbit.annotation.RabbitListener;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Component;
import projeto.pucsp.tcc.core.geolocalizacao.entidade.Endereco;
import projeto.pucsp.tcc.core.geolocalizacao.recurso.EnderecoRecurso;

@Slf4j
@Component
public class EnderecoRecursoImpl implements EnderecoRecurso {

    private final EnderecoRecurso enderecoRecurso;

    public EnderecoRecursoImpl(@Qualifier("enderecoServico") EnderecoRecurso enderecoRecurso) {
        this.enderecoRecurso = enderecoRecurso;
    }

    @Override
    @RabbitListener(queues = "social.geolocalizacao")
    public void processarEnderecoSocial(Endereco endereco) {

        log.info("processar {}", endereco);

        enderecoRecurso.processarEnderecoSocial(endereco);
    }

    @Override
    @RabbitListener(queues = "empresarial.geolocalizacao")
    public void processarEnderecoEmpresarial(Endereco endereco) {

        log.info("processar {}", endereco);

        enderecoRecurso.processarEnderecoEmpresarial(endereco);

    }

}
