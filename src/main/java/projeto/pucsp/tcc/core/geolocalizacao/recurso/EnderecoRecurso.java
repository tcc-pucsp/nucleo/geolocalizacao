package projeto.pucsp.tcc.core.geolocalizacao.recurso;

import projeto.pucsp.tcc.core.geolocalizacao.entidade.Endereco;

public interface EnderecoRecurso {

    void processarEnderecoSocial(Endereco endereco);

    void processarEnderecoEmpresarial(Endereco endereco);

}
