package projeto.pucsp.tcc.core.geolocalizacao.servico;

import org.springframework.stereotype.Service;
import projeto.pucsp.tcc.core.geolocalizacao.entidade.Endereco;
import projeto.pucsp.tcc.core.geolocalizacao.enumeracao.Topico;
import projeto.pucsp.tcc.core.geolocalizacao.recurso.EnderecoRecurso;
import projeto.pucsp.tcc.core.geolocalizacao.recurso.GeolocalizacaoRecurso;

@Service
public class EnderecoServico implements EnderecoRecurso {

    private final GeolocalizacaoRecurso geolocalizacaoRecurso;

    public EnderecoServico(GeolocalizacaoRecurso geolocalizacaoRecurso) {
        this.geolocalizacaoRecurso = geolocalizacaoRecurso;
    }

    private void processarEndereco(Topico topico, Endereco endereco) {

        geolocalizacaoRecurso
                .obterGeoLocalizacao(endereco.toString())
                .log()
                .filter(geolocalizacao -> geolocalizacao.contem(endereco))
                .take(1)
                .map(geolocalizacao -> geolocalizacao.setId(endereco.getId()))
                .log()
                .subscribe(geolocalizacao -> geolocalizacaoRecurso.publicarGeolocalizacao(topico, geolocalizacao));

    }

    @Override
    public void processarEnderecoSocial(Endereco endereco) {

        processarEndereco(Topico.SOCIAL, endereco);

    }

    @Override
    public void processarEnderecoEmpresarial(Endereco endereco) {

        processarEndereco(Topico.EMPRESARIAL, endereco);

    }
}
