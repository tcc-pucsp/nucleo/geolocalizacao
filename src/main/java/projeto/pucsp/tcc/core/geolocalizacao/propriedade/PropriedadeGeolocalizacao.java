package projeto.pucsp.tcc.core.geolocalizacao.propriedade;

import lombok.Getter;
import lombok.Setter;
import lombok.ToString;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.validation.annotation.Validated;

@ToString
@Setter
@Getter
@ConfigurationProperties(prefix = "geolocalizacao")
@Validated
public class PropriedadeGeolocalizacao {

    private String url;

    private String topicExchange;

}
