package projeto.pucsp.tcc.core.geolocalizacao;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.context.properties.EnableConfigurationProperties;
import projeto.pucsp.tcc.core.geolocalizacao.propriedade.PropriedadeGeolocalizacao;

@EnableConfigurationProperties(PropriedadeGeolocalizacao.class)
@SpringBootApplication
public class GeolocalizacaoApplication {

	public static void main(String[] args) {
		SpringApplication.run(GeolocalizacaoApplication.class, args);
	}

}
