package projeto.pucsp.tcc.core.geolocalizacao.enumeracao;

import lombok.AllArgsConstructor;
import lombok.Getter;

@Getter
@AllArgsConstructor
public enum Topico {

    SOCIAL("social.endereco"),
    EMPRESARIAL("empresarial.endereco");

    private final String exchange;

}
