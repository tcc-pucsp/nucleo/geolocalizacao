package projeto.pucsp.tcc.core.geolocalizacao.entidade;

import com.fasterxml.jackson.annotation.JsonAlias;
import lombok.Data;
import lombok.extern.slf4j.Slf4j;

@Slf4j
@Data
public class Geolocalizacao {

    @JsonAlias(value = "lat")
    private String latitude;

    @JsonAlias(value = "lon")
    private String longitude;

    @JsonAlias(value = "display_name")
    private String endereco;

    private Integer id;

    public boolean contem(Endereco endereco) {
        log.info("filtrando endereço {}", endereco);
        return this.endereco.contains(endereco.getCep());
    }

    public Geolocalizacao setId(Integer id) {
        this.id = id;
        return this;
    }

}
