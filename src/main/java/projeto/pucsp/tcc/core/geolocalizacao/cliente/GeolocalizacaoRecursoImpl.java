package projeto.pucsp.tcc.core.geolocalizacao.cliente;

import lombok.extern.slf4j.Slf4j;
import org.springframework.amqp.rabbit.core.RabbitTemplate;
import org.springframework.stereotype.Component;
import org.springframework.web.reactive.function.client.WebClient;
import projeto.pucsp.tcc.core.geolocalizacao.entidade.Geolocalizacao;
import projeto.pucsp.tcc.core.geolocalizacao.propriedade.PropriedadeGeolocalizacao;
import projeto.pucsp.tcc.core.geolocalizacao.recurso.GeolocalizacaoRecurso;
import projeto.pucsp.tcc.core.geolocalizacao.enumeracao.Topico;
import reactor.core.publisher.Flux;

@Slf4j
@Component
public class GeolocalizacaoRecursoImpl implements GeolocalizacaoRecurso {

    private final WebClient webClient;

    private final PropriedadeGeolocalizacao propriedadeGeolocalizacao;

    private final RabbitTemplate rabbitTemplate;

    public GeolocalizacaoRecursoImpl(WebClient webClient, PropriedadeGeolocalizacao propriedadeGeolocalizacao, RabbitTemplate rabbitTemplate) {
        this.webClient = webClient;
        this.propriedadeGeolocalizacao = propriedadeGeolocalizacao;
        this.rabbitTemplate = rabbitTemplate;
    }

    @Override
    public Flux<Geolocalizacao> obterGeoLocalizacao(String endereco) {

        String url = propriedadeGeolocalizacao.getUrl().replace("{ENDERECO}", endereco);

        return webClient
                .get()
                .uri(url)
                .retrieve()
                .bodyToFlux(Geolocalizacao.class);

    }

    @Override
    public void publicarGeolocalizacao(Topico topico, Geolocalizacao geolocalizacao) {

        log.info("publicando {}", geolocalizacao);

        rabbitTemplate.convertAndSend(topico.getExchange(), geolocalizacao);

    }

}
