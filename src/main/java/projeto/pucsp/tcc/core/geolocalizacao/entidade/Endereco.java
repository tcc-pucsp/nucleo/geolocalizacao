package projeto.pucsp.tcc.core.geolocalizacao.entidade;

import lombok.Data;

@Data
public class Endereco {

    private Integer id;

    private String logradouro;

    private String cep;

    private String numero;

    private String cidade;

    private String estado;

    @Override
    public String toString() {
        return logradouro + "," + numero + "," + cidade + "-" + estado + "," + cep;
    }
}
