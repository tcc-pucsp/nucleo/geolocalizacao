package projeto.pucsp.tcc.core.geolocalizacao.recurso;

import projeto.pucsp.tcc.core.geolocalizacao.entidade.Geolocalizacao;
import projeto.pucsp.tcc.core.geolocalizacao.enumeracao.Topico;
import reactor.core.publisher.Flux;

public interface GeolocalizacaoRecurso {

    Flux<Geolocalizacao> obterGeoLocalizacao(String endereco);

    void publicarGeolocalizacao(Topico topico, Geolocalizacao geolocalizacao);
}

